/*!
 * Stacktacular JavaScript Library v1.0.0
 *
 * Copyright 2010, Taylor Marshall
 * Dual licensed under the MIT or GPL Version 2 licenses.
 *
 * Depends on the jQuery JavaScript Library 1.4 or higher
 * http://jquery.com/
 */
(function ($) {
    var API_VERSION = '1.0',
        CACHE_MILLIS = 60 * 1000; // 1 minute
    
    function RequestCache(maxMillisecondsAge) {
        this.maxAge = maxMillisecondsAge;
        this.entries = []; // most recently cached item will be at the end
    }
    
    RequestCache.prototype = {
        /**
         * Adds a request to the cache. Doesn't check that the request exists
         * in the cache, since clients should only add after they failed to
         * find a cached result.
         */
        put: function (url, data) {
            this.entries.push({ url: url, data: data, time: Date.now() });
        },
        
        /**
         * Returns the cached entry if it exists and is not expired, otherwise
         * it will return null. Also clears out any invalid entries.
         */
        get: function (url) {
            var i = this.entries.length,
                expiry = Date.now() - this.maxAge,
                result = null,
                entry;
            while (i--) {
                entry = this.entries[i];
                if (entry.time <= expiry) {
                    // we got to an expired entry, we know all before this are
                    // also expired, let's clear them
                    this.entries.splice(0, i + 1);
                    this.info("Cleared out stale cache entries before index", i);
                    break;
                } else if (!result && url === entry.url) {
                    // if result was found already, we just continue,
                    // since we want to clear out any expired cache entries
                    this.info("Found fresh cache entry for", url);
                    result = entry;
                }
            }
            return result;
        }
    };
    // todo add dummy log methods to the real prototype, move this Mojo code out
    Mojo.Log.addLoggingMethodsToPrototype(RequestCache);
    
    function StackExchange(apiKey) {
        if (apiKey) {
            this.setApiKey(apiKey);
        }
        this.requestCache = new RequestCache(CACHE_MILLIS);
    }
    
    StackExchange.prototype = {
        questions: function (sort, callback) {
            var params = {
                sort: sort,
                pagesize: 20 // TODO make configurable?
            };
            this.get('/questions', params, callback);
        },
        
        question: function (id, callback) {
            var params = {
                answers: true,
                body: true,
                comments: true
            };
            this.get('/questions/' + id, params, callback);
        },
        
        setApiKey: function (key) {
            this.key = key;
        },
        
        // takes an object of the form returned by the stackauth /sites route
        setSite: function (site) {
            this.site = site;
            this.url = [this.site.api_endpoint, API_VERSION].join('/');
        },
        
        // return a list of StackExchange sites
        sites: function (callback) {
            var se = this;
            this._get('http://stackauth.com/' + API_VERSION + '/sites', {}, function (data, textStatus) {
                se.siteList = data.api_sites;
                callback(data, textStatus);
            });
        },
        
        get: function (path, params, callback) {
            // TODO it might be a bad idea to rely on instance variables if the site will be different...
            this._get(this.url + path, params, callback);
        },
        
        _get: function (url, params, callback) {
            var se = this, query, cached;
            if (!params.key && this.key) {
                params.key = this.key;
            }
            query = $.param(params);
            url += (query.length ? '?' + query : '');
            
            // to save resources and to avoid overusing the API, check cache
            cached = this.requestCache.get(url);
            if (cached) {
                callback(cached.data, 'notmodified');
                return;
            }
            
            this.info("[REQUEST]", url);
            
            // TODO right now this will break if Mojo isn't present...
            $.getJSON(url, function (data, textStatus) {
                se.info("got into response");
                se.requestCache.put(url, data);
                if (Mojo.Log.currentLogLevel >= Mojo.Log.LOG_LEVEL_INFO) {
                    se.info("[RESPONSE]", Object.toJSON(data));
                }
                callback(data, textStatus);
            });
        }
    };
    Mojo.Log.addLoggingMethodsToPrototype(StackExchange);
    window.StackExchange = StackExchange;
})(jQuery);
