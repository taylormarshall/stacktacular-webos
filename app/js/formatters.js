Formatters = {
    accepted_status: function (propertyValue, model) {
        if (model.accepted_answer_id || model.accepted) { // could be a question or an answer
            model.accepted_status = 'accepted';
        }
    },
    answer_count: function (propertyValue, model) {
        if (Math.abs(propertyValue) > 999) {
            model.answer_count = Math.floor(propertyValue / 1000) + 'k';
        }
    },
    answered_status: function (propertyValue, model) {
        if (model.answer_count === 0) {
            model.answered_status = 'unanswered';
        }
    },
    answers_label: function (propertyValue, model) {
        model.answers_label = model.answer_count === 1 ? "answer" : "answers";
    },
    score: function (propertyValue, model) {
        if (Math.abs(propertyValue) > 999) {
            model.score = Math.floor(propertyValue / 1000) + 'k';
        }
    },
    tag_list: function (propertyValue, model) {
        if (model.tags && model.tags.length) {
            model.tag_list = '<li>' + model.tags.join('</li> <li>') + '</li>';
        }
    },
    comment_list: function (propertyValue, model) {
        var comments = model.comments;
        if (comments && comments.length) {
            model.comment_list = Mojo.View.render({
                collection: comments,
                template: 'question/comment-list-item'
            });
        }
    },
    owner_block: function (propertyValue, model) {
        if (model.owner) {
            model.owner_block = Mojo.View.render({
                object: model.owner,
                template: 'question/owner-info'
            });
        }
    },
    view_count: function (propertyValue, model) {
        if (Math.abs(propertyValue) > 999) {
            model.view_count = Math.floor(propertyValue / 1000) + 'k';
        }
    },
    views_label: function (propertyValue, model) {
        model.views_label = model.view_count === 1 ? "view" : "views";
    },
    votes_label: function (propertyValue, model) {
        model.votes_label = model.score === 1 ? "vote" : "votes";
    }
};
