function QuestionAssistant(client, question) {
    this.client = client;
    this.question = question;
    this.answersModel = { items:[] };
    //this.viewMenuModel = {
    //    items: [
    //        { label: this.question.title, width: 320 },
    //    ]
    //};
    this.renderQuestion = this.renderQuestion.bind(this);
}

QuestionAssistant.prototype.setup = function () {
    /* this function is for setup tasks that have to happen when the scene is first created */
    
    // TODO do some graceful loading animation, make scene prettier.
		
	/* use Mojo.View.render to render view templates and add them to the scene, if needed */
	jQuery('#question-title').text(this.question.title);
    jQuery('#question-vote-count').text(this.question.score);
    
    
	/* setup widgets here */
	this.controller.setupWidget('answers', {
            itemTemplate: "question/answer-list-item",
            formatters: Formatters,
            hasNoWidgets: true // TODO may not be true if buttons or something gets added
        }, this.answersModel);
	//this.controller.setupWidget(Mojo.Menu.viewMenu, undefined, this.viewMenuModel);
	/* add event handlers to listen to events from widgets */
};

QuestionAssistant.prototype.renderQuestion = function (data, textStatus) {
    var question = data.questions[0],
        answerList = this.controller.get('answers');
    //this.viewMenuModel.items[0].label = question.title;
    //this.controller.modelChanged(this.viewMenuModel, this);
    jQuery('#question').html(Mojo.View.render({
        object: question,
        formatters: Formatters,
        template: 'question/question'
    }));
    answerList.mojo.noticeRemovedItems(0, answerList.mojo.getLength());
    // sort so that accepted answers come first, then rank by votes
    answerList.mojo.noticeAddedItems(0, question.answers.sort(function (a, b) {
        return a.accepted ? -1 : (b.accepted ? 1 : b.score - a.score);
    }));
    this.applySyntaxHighlighting();
};

QuestionAssistant.prototype.applySyntaxHighlighting = function () {
    jQuery('pre').addClass('prettyprint');
    jQuery('a').bind(Mojo.Event.tap, function (event) {
        // TODO if it's to the same site, let's handle the navigation ourselves
        Mojo.Log.error('Handled url:', this.href);
    });
    prettyPrint();
};

QuestionAssistant.prototype.activate = function (event) {
	/* put in event handlers here that should only be in effect when this scene is active. For
	   example, key handlers that are observing the document */
    this.client.question(this.question.question_id, this.renderQuestion);
};

QuestionAssistant.prototype.deactivate = function (event) {
	/* remove any event handlers you added in activate and do any other cleanup that should happen before
	   this scene is popped or another scene is pushed on top */
};

QuestionAssistant.prototype.cleanup = function (event) {
	/* this function should do any cleanup needed before the scene is destroyed as 
	   a result of being popped off the scene stack */
};
