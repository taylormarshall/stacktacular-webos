function SitesAssistant(client) {
    this.client = client;
    this.sitesModel = { items:[] };
    this.sites = [];
    this.filter = this.filter.bind(this);
    this.openSite = this.openSite.bind(this);
    this.populateSiteList = this.populateSiteList.bind(this);
}

SitesAssistant.prototype.setup = function () {
	/* this function is for setup tasks that have to happen when the scene is first created */
		
	/* use Mojo.View.render to render view templates and add them to the scene, if needed */
    
	/* setup widgets here */
    this.controller.setupWidget('sites', {
        itemTemplate: "sites/site-list-item",
        formatters: Formatters,
        hasNoWidgets: true,
        filterFunction: this.filter,
        renderLimit: 100,
        delay: 350
    }, this.sitesModel);
	
	/* add event handlers to listen to events from widgets */
};

SitesAssistant.prototype.openSite = function (event) {
    this.client.setSite(event.item); // TODO this is a bit awkward
    this.controller.stageController.pushScene('main', this.client);
};

SitesAssistant.prototype.filter = function (filterString, listWidget, offset, count) {
    var len = this.sites.length, totalFound = 0, foundItems, site, i, toSearch;
    if (filterString) {
        filterString = filterString.toLowerCase();
        foundItems = [];
        for (i = 0; i < len; ++i) {
            site = this.sites[i];
            toSearch = (site.name + site.description + site.site_url).toLowerCase();
            if (toSearch.indexOf(filterString) >= 0) {
                if (foundItems.length < count && totalFound >= offset) {
                    foundItems.push(site);
                }
                totalFound++;
            }
        }
    } else {
        foundItems = this.sites;
        totalFound = this.sites.length;
    }
    listWidget.mojo.noticeAddedItems(0, foundItems);
    listWidget.mojo.setCount(totalFound);
};

SitesAssistant.prototype.loadSites = function () {
    this.client.sites(this.populateSiteList);
};

SitesAssistant.prototype.populateSiteList = function (data, textStatus) {
    this.sites = data.api_sites;
    this.controller.get('sites').mojo.noticeAddedItems(0, this.sites);
};

SitesAssistant.prototype.activate = function (event) {
    this.controller.listen('sites', Mojo.Event.listTap, this.openSite);
	this.loadSites();
};

SitesAssistant.prototype.deactivate = function (event) {
	/* remove any event handlers you added in activate and do any other cleanup that should happen before
	   this scene is popped or another scene is pushed on top */
    this.controller.stopListening('sites', Mojo.Event.listTap, this.openSite);
};

SitesAssistant.prototype.cleanup = function (event) {
	/* this function should do any cleanup needed before the scene is destroyed as 
	   a result of being popped off the scene stack */
};
