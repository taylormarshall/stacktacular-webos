function StageAssistant() {
    this.client = new StackExchange('key');
}

StageAssistant.prototype.setup = function () {
    this.controller.pushScene('sites', this.client);
};

StageAssistant.prototype.handleCommand = function (event) {
    if (event.type === Mojo.Event.command && event.command === Mojo.Menu.helpCmd) {
        this.controller.pushAppSupportInfoScene();
    } else if (event.type === Mojo.Event.commandEnable && (event.command === Mojo.Menu.helpCmd || event.command === Mojo.Menu.prefsCmd)) {
        event.stopPropagation(); // enables help so we can handle it
    }
};
