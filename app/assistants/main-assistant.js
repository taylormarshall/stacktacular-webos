function MainAssistant(client) {
	this.client = client;
    this.questionsModel = { items:[] };
    this.spinnerModel = { spinning: false };
    this.viewMenuModel = {
        items: [{
            toggleCmd: 'activity',
            items: [
                { label: $L('Active'), width: 105, command: 'activity' },
                { label: $L('Featured'), width: 110, command: 'featured' },
                { label: $L('Hot'), width: 105, command: 'hot' }
            ]
        }]
    };
    this.populateQuestionList = this.populateQuestionList.bind(this);
    this.openQuestion = this.openQuestion.bind(this);
    this.orientationChange = this.orientationChange.bind(this);
    this.currentSort = 'activity';
}

MainAssistant.prototype.setup = function () {
    this.controller.stageController.setWindowOrientation("free");
    this.controller.setupWidget('mainScreenSpinner', {
            spinnerSize: Mojo.Widget.spinnerLarge
        }, this.spinnerModel);
    this.controller.setupWidget(Mojo.Menu.viewMenu, undefined, this.viewMenuModel);
    this.controller.setupWidget('questions', {
            itemTemplate: "main/question-list-item",
            formatters: Formatters,
            hasNoWidgets: true
        }, this.questionsModel);
    this.controller.listen(document, 'orientationchange', this.orientationChange);
    this.controller.listen('questions', Mojo.Event.listTap, this.openQuestion);
};

MainAssistant.prototype.populateQuestionList = function (data, textStatus) {
    var questionList = this.controller.get('questions');
    this.controller.get('mainScreenSpinner').mojo.stop();
    this.controller.get('mainScrim').hide();
    questionList.mojo.noticeRemovedItems(0, questionList.mojo.getLength());
    questionList.mojo.noticeAddedItems(0, data.questions);
};

MainAssistant.prototype.openQuestion = function (event) {
    this.controller.stageController.pushScene('question', this.client, event.item);
};

MainAssistant.prototype.loadQuestions = function () {
    this.controller.get('mainScrim').show();
    this.controller.get('mainScreenSpinner').mojo.start();
    this.client.questions(this.currentSort, this.populateQuestionList);
};

MainAssistant.prototype.handleCommand = function (event) {
    if (event.type === Mojo.Event.command) {
        switch (event.command) {
            case 'activity': case 'hot': case 'featured':
                this.currentSort = event.command;
                this.loadQuestions();
                break;
            default:
                Mojo.Log.warn("Got unexpected command", event.command);
        }
    }
};

MainAssistant.prototype.orientationChange = function (event) {
    if (event.position > 3) {
        this.controller.setMenuVisible(Mojo.Menu.viewMenu, false);
    } else if (event.position > 1) {
        this.controller.setMenuVisible(Mojo.Menu.viewMenu, true);
    }
};

MainAssistant.prototype.activate = function (event) {
	/* put in event handlers here that should only be in effect when this scene is active. For
	   example, key handlers that are observing the document */
    this.loadQuestions();
};

MainAssistant.prototype.deactivate = function (event) {
	/* remove any event handlers you added in activate and do any other cleanup that should happen before
	   this scene is popped or another scene is pushed on top */
};

MainAssistant.prototype.cleanup = function (event) {
	/* this function should do any cleanup needed before the scene is destroyed as 
	   a result of being popped off the scene stack */
    this.controller.stopListening(document, 'orientationchange', this.orientationChange);
};

